import numpy as np
import pandas as pd
import sys

from sklearn.decomposition import PCA
from scipy.cluster.vq import kmeans,vq,whiten

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# load the matrix
df = pd.read_csv('distance_matrix4.tsv', sep='\t',
        header=0, index_col=0)

# perform pca to reduce features to 2 components (2D)

targets = list(df.columns)

pca = PCA(n_components=2)

principalComponents = pca.fit_transform(df)

pcdf = pd.DataFrame(principalComponents,
        columns=['pc1','pc2'])


# perform k-means on the pcdf

# computing K-Means with K = 5 
centroids,distortion = kmeans(pcdf,5)

# assign each sample to a cluster
# idx[i] is the cluster(0,1,2) to which mogsem_i belongs.

idx,distances = vq(pcdf,centroids)


pcdf

pcdf[idx==0]['pc1']

pcdf['cluster'] = idx
pcdf
idx

xs = pcdf['pc1']
ys = pcdf['pc2']

# plotting
#plot = plt.plot(pcdf[idx==0]['pc1'],pcdf[idx==0]['pc2'],'ob',
#     pcdf[idx==1]['pc1'],pcdf[idx==1]['pc2'],'or',
#     pcdf[idx==2]['pc1'],pcdf[idx==2]['pc2'],'om',
#     pcdf[idx==3]['pc1'],pcdf[idx==3]['pc2'],'oy',
#     pcdf[idx==4]['pc1'],pcdf[idx==4]['pc2'],'oc')
#plt.xlabel('pc1')
#plt.ylabel('pc2')
#
#colours = ListedColormap(['r','b','g','m','c', 'y'])
#colours

pcdf['cluster']

scatter = plt.scatter(pcdf['pc1'],
        pcdf['pc2'],c=list(pcdf['cluster']), cmap=colours)

#plt.legend(handles=scatter.legend_elements()[0],
#labels=['c0','c1','c2','c3','c4'])

plt.xlabel('pc1')
plt.ylabel('pc2')

cs = pcdf['cluster']
i = 1
for xy in zip(xs, ys):                                      
    plt.annotate('%s' %i, xy=xy, textcoords='data')
    i += 1

plt.title('K means clustering of the 2 principle components')

#plt.plot(centroids[:,0],centroids[:,1],'sm',markersize=8)


plt.show()

plt.close()

