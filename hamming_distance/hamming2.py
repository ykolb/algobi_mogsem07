import numpy as np
import pandas as pd
import sys

columns =  "CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,FORMAT,\
mogsem01,mogsem02,mogsem03,mogsem04,mogsem05,\
mogsem06,mogsem07,mogsem09,mogsem10,mogsem11,\
mogsem12,mogsem13,mogsem14,mogsem15,mogsem16,mogsem17".split(',')

# This calculations depends on a merged vcf file of all groups.
# To prepare such file: each original vcf file must contain a unique
# group name (inour case it was '1', it's the 
# column right after FORMAT).
# Then gz the files and make the merged file
# then for convinience remove all the flags out of the FORMAT column
# and leave just the genptype GT, for that I used:
# 'bcftools annotate -x ^INFO/AC,^FORMAT/GT mergedsNPs.vcf > mergedSNP_simplified.vcf'
# The advantage of the merge is that it give all the positions that
# appear in any of the individual vcfs. The genotype for individual
# gX in a position not listed in its own vcf would be './.' which is
# equivalent to '0/0' which means both alleles are equal the
# reference alleles (0).

# replace it later with sys.argv[1] or other arg
#vcfpath = 'mergedSNP_simplified.vcf'

#vcfpath = '../rawSNPs/mergedsSnps.vcf'
vcfpath = '~/FU/AlgoBi_Prak/algobi_project/allvcf/mergedSNP_simplified.vcf'

#df = pd.read_table(vcfpath, header=None,
#        comment='#',
#        names=columns)
#
df = pd.read_csv(vcfpath, header=None,
        sep='\t',
        comment='#',
        names=columns)

#(df['ID'] != '.').sum() # how many unsusual IDs, consider removing
                        # them
#l = df['mogsem05'] > '3' 

# for tests
#smalldf = df[columns[9:]].iloc[0:10]

#smalldf
#f = lambda s: s.replace('.','0').split('/')
#pdiff = lambda x: lambda y: (
#    len([elem for elem in x if elem not in y])
#    )


def hamming_old(df, g1, g2):
    """Calculates Hamming distance between
    individuals (genotype columns) 'g1' and 'g2' of the
    data frame df"""
    # replace '.' with '0' (the ref allel)
    # and narrow to down df to just the two
    # relevant columns:
    h = lambda s: (s.replace('.','0').split('/'))
    # like h but turns the lists into sets
    f = lambda s: set(s.replace('.','0').split('/'))
    positions = df[g1] != df[g2]
    x = np.vectorize(f)(df[g1][positions]) #allel sets
    xl = np.vectorize(len)(x) #allel number (1 or 2)
    y = np.vectorize(f)(df[g2][positions])
    yl = np.vectorize(len)(y) #allel number (1 or 2)
    #also create the union sets:
    z = np.vectorize(f)(df[g1][positions] + "/" + df[g2][positions])
    zl = np.vectorize(len)(z) #allel number (1 or 2)
    #positions = x != y
    # case of 2 unequal homos: d=2
    d = 2 * ((x != y)*(xl == 1)*(yl == 1)).sum() 
    # case of 2 heteros with 1 common: d=1
    d += ((xl == 2)*(yl == 2)*(zl == 3)).sum()
    # case of 2 heteros with 0 common: d=2
    d += 2*((xl == 2)*(yl == 2)*(zl ==4)).sum() 
    # case of homo,hetero with common base: d=1
    d += ((xl != yl)*(zl == 2)).sum() 
    # case of hetero,homo without common base: d=2
    d += 2*((xl != yl)*(zl == 3)).sum() 
    return d

def hamming(df, g1, g2):
    """Calculates Hamming distance between
    individuals (genotype columns) 'g1' and 'g2' of the
    data frame df"""
    # replace '.' with '0' (the ref allel)
    # and narrow to down df to just the two
    # relevant columns:
    if (df[g1] != df[g2]).sum() == 0:
        return 0
    h = lambda s: (s.replace('.','0').split('/'))
    # like h but turns the lists into sets
    f = lambda s: set(s.replace('.','0').split('/'))
    ddf = df[df[g1] != df[g2]]
    x = np.vectorize(f)(ddf[g1]) #allel sets
    xl = np.vectorize(len)(x) #allel number (1 or 2)
    y = np.vectorize(f)(ddf[g2])
    yl = np.vectorize(len)(y) #allel number (1 or 2)
    #also create the union sets:
    z = np.vectorize(f)(ddf[g1] + "/" + ddf[g2])
    zl = np.vectorize(len)(z) #allel number (1 or 2)
    # case of 2 unequal homos: d=2
    d = 2 * ((x != y)*(xl == 1)*(yl == 1)).sum() 
    # case of 2 heteros with 1 common: d=1
    d += ((xl == 2)*(yl == 2)*(zl == 3)).sum()
    # case of 2 heteros with 0 common: d=2
    d += 2*((xl == 2)*(yl == 2)*(zl ==4)).sum() 
    # case of homo,hetero with common base: d=1
    d += ((xl != yl)*(zl == 2)).sum() 
    # case of hetero,homo without common base: d=2
    d += 2*((xl != yl)*(zl == 3)).sum() 
    return d

# examples
print("Example: hamming distance between group07 and 03",
    hamming(df, 'mogsem07', 'mogsem03'),
    "hamming distance between group 7 and itself:",
    hamming(df, 'mogsem07', 'mogsem07'))

groups = columns[9:] #all the groups

groups

n = len(groups)

distance_matrix = np.zeros((n,n))

#print('calculating distance matrix, takes a long time...') 
#for i in range(n):
#    for j in range(i):
#        distance_matrix[i,j] = hamming(df, groups[i], groups[j])
#
print(distance_matrix)

smalldf = df[columns[9:]].iloc[0:10]
smalldf
testdf = smalldf[columns[9:16]].iloc[0:3]
smalldf

groups
rt = np.array(
        "0/0,0/1,1/0,1/1,0/2,0/3,2/3".split(','))
testdf.iloc[1] = rt
testdf
testdf.iloc[1]
for i in range(7):
    print("group1 vs:" , i,"\n",  hamming(testdf, groups[1],groups[i]))


#### Build the Distance Matrix of the raw snps:
print('calculating distance matrix, takes a long time...') 
for i in range(n):
    for j in range(i):
        distance_matrix[i,j] = hamming(df, groups[i], groups[j])

myfile = open('rawmatrix', 'w')
print(distance_matrix, file=myfile)

test = pd.DataFrame(distance_matrix, columns=groups)
test.to_csv('distance_matrix.tsv', sep='\t', header=True)

print(distance_matrix)

