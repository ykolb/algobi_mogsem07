#!/bin/bash
#for file in "$@"; do
#	#bgzip "$file"
#	#tabix "$file.gz"
#done

# run the script with a parameter files.txt which
# is the path for a file that contains a list of all
# the .vcf.gz files to merge:
# ./merge_vcfgz.sh files.txt
# or just use the command itself instead of the script...

bcftools merge -l "$@" -o "mergedsSnps.vcf" 

