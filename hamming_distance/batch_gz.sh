#!/bin/bash
# to run it on all the vcf in the folder:
# ./merge_vcfgz *.vcf
# BEFORE you run this script, make sure that each vcf has a unique
# group identifyer. look for the last commented line in the vcf, it
# looks like this:
# 
for file in "$@"; do
	bgzip "$file"
	tabix "$file.gz"
done

# after that, create a list of all the *.vcf.gz files and
# bcftools merge -l files.txt -o "mergedsSnps.vcf". if it looks like
# this:
#
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO FORMAT 1
#
# Then change the group ID to a unique name like this:
#
#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT mogsem07

