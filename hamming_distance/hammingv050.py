import numpy as np
import pandas as pd
import sys

columns =  "CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,FORMAT,\
mogsem01,mogsem02,mogsem03,mogsem04,mogsem05,\
mogsem06,mogsem07,mogsem08,mogsem09,mogsem10,mogsem11,\
mogsem12,mogsem13,mogsem14,mogsem15,mogsem16,mogsem17".split(',')

# group keyes:
groups = columns[9:]

# This calculations depends on a merged vcf file of all groups.
# To prepare such file: each original vcf file must contain a unique
# group name (inour case it was '1', it's the 
# column right after FORMAT).
# Then gz the files and make the merged file
# then for convinience remove all the flags out of the FORMAT column
# and leave just the genptype GT, for that I used:
# 'bcftools annotate -x ^INFO/AC,^FORMAT/GT mergedsNPs.vcf > mergedSNP_simplified.vcf'
# The advantage of the merge is that it give all the positions that
# appear in any of the individual vcfs. The genotype for individual
# gX in a position not listed in its own vcf would be './.' which is
# equivalent to '0/0' which means both alleles are equal the
# reference alleles (0).

# replace it later with sys.argv[1] or other arg

rawSNPspath = '~/FU/AlgoBi_Prak/algobi_project/rawSNPs/simplified_merged_rawSNPs.vcf'

df = pd.read_csv(rawSNPspath, header=None,
        sep='\t',
        comment='#',
        names=columns)

###########################################################
#################################################

def hamming(df, g1, g2):
    """Calculates Hamming distance between
    individuals (genotype columns) 'g1' and 'g2' of the
    data frame df"""
    # replace '.' with '0' (the ref allel)
    # and narrow to down df to just the two
    # relevant columns:
    ddf = df[df[g1] != df[g2]]
    if (ddf[g1] != ddf[g2]).sum() == 0:
        return 0
    #h = lambda s: (s.replace('.','0').replace('|','/').split('/'))
    # like h but turns the lists into sets
    f = lambda s: set(s.replace('.','0').replace('|','/').split('/'))
    x = np.vectorize(f)(ddf[g1]) #allel sets
    xl = np.vectorize(len)(x) #allel number (1 or 2)
    y = np.vectorize(f)(ddf[g2])
    yl = np.vectorize(len)(y) #allel number (1 or 2)
    #also create the union sets:
    z = np.vectorize(f)(ddf[g1] + "/" + ddf[g2])
    zl = np.vectorize(len)(z) #allel number (1 or 2)
    # case of 2 unequal homos: d=2
    d = 2 * ((x != y)*(xl == 1)*(yl == 1)).sum() 
    # case of 2 heteros with 1 common: d=1
    d += ((xl == 2)*(yl == 2)*(zl == 3)).sum()
    # case of 2 heteros with 0 common: d=2
    d += 2*((xl == 2)*(yl == 2)*(zl ==4)).sum() 
    # case of homo,hetero with common base: d=1
    d += ((xl != yl)*(zl == 2)).sum() 
    # case of hetero,homo without common base: d=2
    d += 2*((xl != yl)*(zl == 3)).sum() 
    return d

##################################################################
########################################################################3

#### Build the Distance Matrix of the raw snps:
n = len(groups)
distance_matrix = np.zeros((n,n))
print('calculating distance matrix, takes a long time...') 
for i in range(n):
    for j in range(i):
        distance_matrix[i,j] = hamming(df, groups[i], groups[j])
        #distance_matrix[j,i] = distance_matrix[i,j]

distance_matrix[1,0]

distance_matrix = distance_matrix.transpose() + distance_matrix

myfile = open('rawmatrix3', 'w')
print(distance_matrix, file=myfile)
myfile.close()

test = pd.DataFrame(distance_matrix, columns=groups)
test.to_csv('distance_matrix4.tsv', sep='\t', header=True)

print(distance_matrix)

ttt= pd.read_csv('distance_matrix3.tsv',
        sep='\t', header=0, index_col=0)

################################################################################
### 1000 Genomes ###
# exctract chr1 of our sample mogsem07 with bcftools view -r chr1
# exclude all 0|0 lines out of the 1000 genome file:
# bcftools view -e 'GT="0|0"' file.vcf > new.vcf
# merge the narrowed down sample genomes with our mogsem07 sample:
# bcftools merge f1.gz f2.gz -o f3.vcf
# and again remove everithing from the format field except GT using
# bcftools annotate -x ^FORMAT/GT file.vcf > output.bcf

def distmatrix(df, groups):
    """given a vcf data frame and list of columns (samples) it
    returns the hamming distance matrix"""
    n = len(groups)
    distance_matrix = np.zeros((n,n))
    for i in range(n):
        for j in range(i):
            distance_matrix[i,j] = hamming(df, groups[i], groups[j])
    return distance_matrix


header = "CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,FORMAT,\
mogsem07,NA18972,NA19002,NA19007,NA18631,NA18530,NA18539,NA18877,\
NA18488,NA18907,NA19020,NA19310,NA19036,HG03644,HG03692,HG03740,\
HG01593,HG02687,HG02601,HG01571,HG01893,HG01932,HG00102,HG00253,\
HG00143,HG00268,HG00181,HG00280".split(',')

# This file is the complete merge, where missing positions in one
# file are imputed as 0/0 by the distance calculations.
# But It might be better to just use the common positions in the
# other file:
vcffile = '../../refVCF/vcf/merged_chr1_mogsem07_1000G_simplified.vcf'

# This newer file only contains position that are common to both
# files
vcffile = '../../refVCF/vcf/testisec/common_merge_simple.vcf'

df2 = pd.read_csv(vcffile, header=None,
        sep='\t',
        comment='#',
        names=header)

# distance matrix between mogsem07 and the 1000G sample:
m = len(header[9:])

print('calculating distance matrix, takes a long time...') 
k_matrix = distmatrix(df2, header[9:])

k_matrix += k_matrix.transpose()

test2 = pd.DataFrame(k_matrix, columns=header[9:])
#test2.to_csv('100G_distance_matrix2.tsv', sep='\t', header=True)
test2.to_csv('100G_distance_matrix_only_common_positions.tsv', sep='\t',
        header=True, index=False)

test1 = pd.read_csv('100G_distance_matrix2.tsv', 
        sep='\t', header=0)

myfile = open('raw1000gmatrix2', 'w')
print(k_matrix, file=myfile)
myfile.close()

###############################################
### Preparation of Data for PCA
### Preparation of the Data:
# First an intersect is created between the mogsem07.vcf and the
# sample file. The exact command is:
# bcftools isec file1.vcf file2.vcf -p outputdir
# in the output dir the last two file 0002 and 0003 contain the
# common positions of both files, so we gz and index them and then merge 
# them with
# bcftools merge file1 file2 -o outputfile
# The reason for the intersect is that there are two many positions
# that are not common between the files and if left that way the
# mogsem comes out as completely different and the other samples in
# comparison seems extremely similar, in the PCA.
# We than simplify the merged file by rmoving INFO field and leaving
# just FORMAT/GT (again with bcftools annotate, see scripts)
# In addition we added a "mogsem" type of "population" to the
# population table (the gfile below)


gfile = "../../refVCF/vcf/mogsem.tsv"

#vcffile = "../../refVCF/vcf/merged_chr1_mogsem07_1000G_simplified.vcf"

header = "CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,FORMAT,\
NA18972,NA19002,NA19007,NA18631,NA18530,NA18539,NA18877,\
NA18488,NA18907,NA19020,NA19310,NA19036,HG03644,HG03692,HG03740,\
HG01593,HG02687,HG02601,HG01571,HG01893,HG01932,HG00102,HG00253,\
HG00143,HG00268,HG00181,HG00280,mogsem07".split(',')

vcffile = '../../refVCF/vcf/testisec/common_merge_simple.vcf'


group_data = pd.read_csv(gfile, sep='\t', header=0)

sampledf = pd.read_csv(vcffile, sep='\t', comment='#',
        header=None, names=header)

group_data
sampledf

sampledf[header[9:]]
 
# we turn the snp data to integers 2,1 r 0 (0,1,2 snp allels)
gg = lambda s: (s.replace('.','').replace('|','')).replace(
        '0','').replace('/','')

test = np.vectorize(len)(
    np.vectorize(gg)(sampledf[header[9:]])
    )

sampledf[header[9:]] = test

sampledf

# save the dataFrame
sampledf.to_csv('snp_array_for_pcs.2.tsv',
        sep='\t', index=False, header='true')

# prepare also an inverted data Frame. Because the PCA function
# assumes that the features are the columns and the variables
# (samples) are rows. It also have the added advantage that we can
# add the target (=population group) as an additional column.
invdf = sampledf.transpose().iloc[9:]

group_data.index = group_data['Sample name']

namelist = group_data.loc[header[9:]]['Superpopulation name']

namelist #match sample with its population group

# add the target column
invdf['target'] = namelist

invdf

invdf.shape

# save
invdf.to_csv('inverted_snp_tab.tsv', sep='\t',
        header=True, index=True)

####################### PCA #####################
# https://towardsdatascience.com/pca-using-python-scikit-learn-e653f8989e60

from sklearn.preprocessing import StandardScaler

features = invdf.columns[:-1]

features

# Separating out the features
x = invdf.loc[:, features].values

# Separating out the target
y = invdf.loc[:,['target']].values

# Standardizing the features
x = StandardScaler().fit_transform(x)

from sklearn.decomposition import PCA

pca = PCA(n_components=2)

principalComponents = pca.fit_transform(x)

principalDf = pd.DataFrame(data = principalComponents
             , columns = ['principal component 1', 'principal component 2'])


principalDf.index = invdf.index

finalDf = pd.concat([principalDf, invdf[['target']]], axis = 1)

principalDf

finalDf

# Save the PCs
finalDf.to_csv('finalDF', sep='\t',
        header=True, index=True)

finalDf = pd.read_csv('finalDF', sep='\t',
        index_col=0, header=0)


# Plotting

import matplotlib.pyplot as plt

#fig = plt.figure(figsize = (8,8))
#finalDf

plt.scatter(xs,ys, c=colors)

#ax.set_xlabel('Principal Component 1', fontsize = 15)
#ax.set_ylabel('Principal Component 2', fontsize = 15)
#ax.set_title('2 component PCA', fontsize = 20)


targets = list(finalDf['target'])

unames = np.unique(targets)

colorcode = list(np.arange(0,len(unames)))

#colorcode = ['r','g','b','c','m','y']
#b : blue.
#g : green.
#r : red.
#c : cyan.
#m : magenta.
#y : yellow.
#k : black.
#w : white.

#ax = fig.add_subplot(1,1,1) 

xs = finalDf['principal component 1']
ys = finalDf['principal component 2']

colordict = dict(zip(unames, colorcode))

colors = [colordict[x] for x in targets]

colors

#ax.scatter(xs,ys, c=colors, label=targets)
#ax.legend(targets)
#ax.legend(targets)
#ax.grid()
#ax.legend(targets)
#plt.show()
#plt.legend(targets)
#plt.close()


import matplotlib.pyplot as plt

from matplotlib.colors import ListedColormap


classes = list(unames)

classes
values = colors
colours = ListedColormap(['r','b','g','m','c','y'])

scatter = plt.scatter(xs, ys,c=values, cmap=colours)
plt.legend(handles=scatter.legend_elements()[0], labels=classes)

plt.xlabel('pc1')
plt.ylabel('pc2')

plt.show()





