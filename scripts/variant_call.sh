#!/bin/bash
gatk --java-options "-Xmx100g" HaplotypeCaller \
	-R /project/pcpool_data/genome/hg38.fa \
	-I /project/pcpool_data/gruppen/mogsem07/Projekt2020/newra.bam \
	-O /project/pcpool_data/gruppen/mogsem07/Projekt2020/newra.vcf \
	--native-pair-hmm-threads 64
