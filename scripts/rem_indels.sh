#!/bin/bash
vcftools --vcf "$@" --remove-indels --recode --recode-INFO-all --out SNPs_only.vcf
