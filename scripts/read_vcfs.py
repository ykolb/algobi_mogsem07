import sys
import pandas as pd

def get_filename(number) :
    prefix = '0' if number < 10 else ''
    return '/project/pcpool_data/rawSNPs/mogsem' + prefix + str(number) +  '.rawSNPs.vcf'

def main() :
    vcfs = []
    for i in range(1,18) :
        vcfs.append( pd.read_csv(get_filename(i), comment='#', header = None, sep = '\t', usecols = [0,1,3,4,7]) )
        print(i)

if __name__ == '__main__' :
    main()
